<?php

$option_companyName = get_field( 'option_company_name', 'option' );
$option_companyDescription = get_field( 'option_company_description', 'option' );
$option_address       = get_field( 'option_address', 'option' );
$option_postcode     = get_field( 'option_postcode', 'option' );
$option_city         = get_field( 'option_city', 'option' );
$option_mail         = get_field( 'option_mail', 'option' );
$option_tel          = get_field( 'option_tel', 'option' );

?>

<!-- footer top -->
<div class="row prefooter">
    <!-- left side -->
    <div class="col-12 text-center col-md-5 col-lg-6 text-md-left col-xl-6">
        <div class="prefooter__left-address ">
            <div class="prefooter__left-address-top "><?php echo $option_companyDescription; ?></div>
            <div class="prefooter__left-address-bottom">
                <p><?php echo $option_companyName; ?></p>
                <ul>
                    <li><?php echo $option_postcode; ?> <?php echo $option_city; ?></li>
                    <li>
                        <a href="mailto:<?php echo $option_mail; ?>">
                            <?php echo $option_mail; ?>
                        </a>
                    </li>
                    <li>
                        <a href="tel:<?php echo $option_tel; ?>">
                            <?php echo $option_tel; ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- right side -->
    <div class="col-12 text-center col-md-7 col-lg-6 col-xl-6 text-md-left  ">
        <div class="prefooter__right d-flex ">
            <div class="prefooter__right-item col-md-4">
                <?php
                    wp_nav_menu(array(
                    'theme_location' => 'menu-2',
                    'menu_id' => 'footer-menu-1',
                     ));?>
            </div>
            <div class="prefooter__right-item col-md-4">
                <?php
                    wp_nav_menu(array(
                    'theme_location' => 'menu-3',
                    'menu_id' => 'footer-menu-2',
                     ));?>
            </div>
            <div class="prefooter__right-item col-md-4">
                <?php
                    wp_nav_menu(array(
                    'theme_location' => 'menu-4',
                    'menu_id' => 'footer-menu-3',
                     ));?>
            </div>
        </div>
    </div>
</div>