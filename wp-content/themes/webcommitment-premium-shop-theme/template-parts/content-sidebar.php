<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 26/05/2020
 * Time: 11:06
 */

?>

<aside class="shop__aside-wrapper">
    <div class="sidebar-close-btn">
		<?php get_template_part( 'img/icons/close.svg' ); ?>
    </div>
	<?php
	/**
	 * Hook: woocommerce_sidebar.
	 *
	 * @hooked woocommerce_get_sidebar - 10
	 */
	do_action( 'woocommerce_sidebar' );
	?>


</aside>

