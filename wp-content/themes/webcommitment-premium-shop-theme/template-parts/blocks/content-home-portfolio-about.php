<?php
$sub_title = get_field('home_about_sub-title');
$title = get_field('home_about_title');
$content = get_field('home_about_content');
$link = get_field('home_about_link');
$img1 = get_field('home_about_image_1');
$img2 = get_field('home_about_image_2');
?>

<section class="home-portfolio-about">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="home-portfolio-about__left">
                    <div class="home-portfolio-about__portfolio">
                        <div class="home-portfolio">
                            <div class="home-portfolio__items">
                                <div class="home-portfolio__item animate-2">
                                    <img src="<?php echo $img1 ['sizes']['wc-page-header'];?>" alt="">
                                </div>
                                <div class="home-portfolio__item animate-2">
                                    <img src="<?php echo $img2 ['sizes']['wc-page-header'];?>" alt="">
                                </div>
                            </div>
                            <?php $portfolio = get_field('home_about_portfolio_introduction');
                            if ($portfolio ) : ?>
                            <div class="home-portfolio__banner animate-2">
                                <div class="home-portfolio__banner-icon">
                                    <img src="<?php echo $portfolio[ 'icon' ]; ?>" alt="">
                                </div>

                                <p><?php echo $portfolio[ 'content' ]; ?></p>
                                <a class="primary-btn" href="<?php echo esc_url($portfolio['button']['url']) ; ?>">
                                    <?php echo $portfolio['button']['title']; ?></a>
                            </div>

                            <?php endif;?>
                        </div>
                    </div>
                </div>

                <div class="home-portfolio-about__right">
                    <div class="home-about">
                        <?php if (!empty($sub_title)): ?>
                        <h5 class="home-about__sub-title animate-3">
                            <?php echo $sub_title; ?>
                        </h5>
                        <?php endif; ?>

                        <?php if (!empty($title)): ?>
                        <h2 class="home-about__title animate-4 ">
                            <?php echo $title; ?>
                        </h2>
                        <?php endif; ?>

                        <?php if (!empty($content)): ?>
                        <div class="home-about__content animate-2 list">
                            <?php echo $content; ?>
                        </div>
                        <?php endif; ?>

                        <?php if (!empty($link)): ?>
                        <a class="home-about__link primary-btn" href="<?php echo $link['url']; ?>">
                            <?php echo $link['title']; ?>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>