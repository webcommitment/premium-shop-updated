<?php
$categories = get_terms(
    'product_cat',
    array('parent' => 0)
);
?>

<?php if (!empty($categories)) : ?>
    <section class="home-categories">
        <div class="container-fluid">
            <div class="row align-items-left  flex-column-reverse flex-sm-column-reverse flex-lg-row">
                <div class="col-12 col-lg-6 col-xl-6 ">
                    <div class="row home-categories__left">
                        <?php foreach ($categories as $category):
                            $term_id = $category->term_id;
                            $featured_cat = get_field('featured_category', $category);

                            if ($featured_cat) :
                                $term_name = $category->name;
                                $term_description = $category->description;
                                $thumbnail_id = get_term_meta($category->term_id, 'thumbnail_id', true);
                                $term_image = wp_get_attachment_url($thumbnail_id);
                                $term_link = get_term_link($term_id);
                                ?>
                                <div class="col-10 col-md-5 col-card home-categories__item animate-2">
                                    <div class="home-categories__card">
                                        <div class="home-categories__thumbnail-wrapper">
                                            <a href="<?php echo $term_link; ?>">
                                                <?php if (!empty ($term_image)): ?>
                                                    <img src="<?php echo $term_image; ?>"
                                                         alt="term_image_<?php echo $term_id; ?>">
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                        <div class="home-categories__content-wrapper">
                                            <div class="home-categories__content-title">
                                                <h5>
                                                    <?php echo $term_name; ?>
                                                </h5>
                                            </div>
                                            <div class="home-categories__content-description">
                                                <p>
                                                    <?php echo $term_description; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <?php
                            endif;
                        endforeach; ?>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-6 ">
                    <div class="row home-categories__right">
                        <?php while (have_rows('our_categories')) : the_row(); ?>
                            <div class="col-12 col-md-10 ">
                                <h5 class="home-categories__sub-title animate-3"><?php echo get_sub_field('subtitle') ?></h5>
                                <h2 class="animate-4"><?php echo get_sub_field('main_title'); ?></h2>
                                <div class="home-categories__content">
                                    <?php echo get_sub_field('main_description'); ?>
                                    <span>
                             <?php echo get_sub_field('secondary_description'); ?>
                            </span>
                                </div>
                                <div>
                                    <a href="/shop/" class="primary-btn">
                                        <span><?php echo get_sub_field('button_title'); ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;