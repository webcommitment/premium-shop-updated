<?php
$products = Premium_Core_Shop::get_featured_products();
if ($products->have_posts()) : ?>
    <section class="product-item">
        <div class="container-fluid">
            <div class="row">
                <?php while ($products->have_posts()): ?>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <?php
                        $products->the_post();
                        get_template_part('template-parts/blocks/content', 'product-card'); ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                <?php endwhile; ?>
            </div>

            <div class="d-flex justify-content-center">
                <a class="primary-btn" href="<?php echo get_post_type_archive_link('product'); ?>
                ">
                    <?php echo __('Zie alle producten', 'webcommitment-theme'); ?></a>
            </div>

        </div>
    </section>
<?php endif; ?>


