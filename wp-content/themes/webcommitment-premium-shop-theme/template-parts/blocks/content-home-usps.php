<?php if (have_rows('home_usps')): ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <section class="home-usps">
                    <?php while (have_rows('home_usps')):
                        the_row();
                        $icon = get_sub_field('icon');
                        $title = get_sub_field('title');
                        $content = get_sub_field('excerpt');
                        ?>
                        <div class="home-usps__item">
                            <div class="home-usps__icon">
                                <img src="<?php echo $icon['url']; ?>"
                                     alt="<?php echo $title; ?>"/>
                            </div>
                            <div class="home-usps__content">
                                <h3 class="home-usps__title">
                                    <?php echo $title; ?>
                                </h3>
                                <div class="home-usps__excerpt">
                                    <?php echo $content; ?>
                                </div>
                                </d>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </section>
            </div>
        </div>
    </div>
<?php endif; ?>