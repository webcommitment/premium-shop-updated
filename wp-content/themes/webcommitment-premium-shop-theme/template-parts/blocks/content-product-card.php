<?php
global $product;
?>

<a href="<?php echo get_permalink($product->ID); ?>">
    <div class="product-item__product-card">
        <div class="product-item__image">
            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($product->ID), 'single-post-thumbnail'); ?>
            <img src="<?php echo $image[0]; ?>" data-id="<?php echo $product->post->ID; ?>">
        </div>
        <div class="product-item__details">
            <h3 class="product-item__title">
                <?php echo get_the_title($product->post->ID); ?>
            </h3>

            <div class="product-item__price">
                <?php
                $price = $product->get_price();
                $regular_price = $product->get_regular_price();
                $sale_price = $product->get_sale_price();
                ?>

                <?php if (!$sale_price) : ?>
                    <span class="product-item__price--initial">
                        <span class="currency"><?php echo get_woocommerce_currency_symbol(); ?></span>
                        <span class="int"><?php echo $price; ?></span>
                    </span>
                <?php endif; ?>

                <?php if ($sale_price) : ?>
                    <span class="product-item__price--sale">
                        <span class="currency"><?php echo get_woocommerce_currency_symbol(); ?></span>
                        <span class="int"><?php echo $sale_price; ?></span>
                    </span>

                    <span class="product-item__price--regular">
                        <span class="currency"><?php echo get_woocommerce_currency_symbol(); ?></span>
                        <span class="int"><?php echo $regular_price; ?></span>
                    </span>
                <?php endif; ?>
            </div>
        </div>

        <?php if ($sale_price) : ?>
            <div class="product-item__discount">
                <?php
                $discount = Premium_Core_Shop::get_discount_percentage();
                echo '-' . $discount . '%';
                ?>
            </div>
        <?php endif; ?>

        <div class="product-item__button">
            <?php
            // $product_id = get_product(ID);
            echo "<a class='primary-btn primary-btn--cart' href='" . $product->add_to_cart_url() . "'>Add to cart</a>"; ?>
        </div>
    </div>
</a>