<section class="with-img">
    <div class="d-flex row">
        <?php if ( !empty ( 'content_with_slider' ) ) : ?>
        <div class="col-12 col-lg-6">
            <div class="with-img__left">
                <div class="with-img__content">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div id="page-slider" class="with-img__right carousel slide carousel-fade" data-ride="carousel">
                <?php if ( have_rows ( 'content_slider' ) ) : ?>
                <div class="carousel-inner">
                    <?php
                $l = 0; ?>
                    <?php while (have_rows('content_slider')) : the_row();
                    $slide_image = get_sub_field('slide');?>
                    <div
                        class=" col-12 col-sm-12 col-md-11 col-lg-12 col-xl-12 carousel-item <?php echo($l == 0 ? 'active' : ''); ?>  px-0">
                        <div class="with-img__right-image">
                            <img src="<?php echo $slide_image['sizes']['wc-page-header'] ?>" alt="">
                        </div>
                        <div class="home-banner__carousel-nav">
                            <a class="carousel-control-prev disable-anim" href="#page-slider" role="button"
                                data-slide="prev">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next disable-anim" href="#page-slider" role="button"
                                data-slide="next">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <?php 
                    $l++;
                    endwhile;
                    wp_reset_postdata();
                    ?>
                    <!-- container with icons -->
                </div>
                <?php endif; ?>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0">
                    <div class="with-img__right-icons ">
                        <?php if (have_rows('main_ctas', 'option')): ?>
                        <div class="">
                            <h3>
                                <?php echo __('Neem contact met ons op', 'webcommitment-theme'); ?>
                            </h3>
                            <div class="newsletter__icons">
                                <?php while (have_rows('main_ctas', 'option')): the_row();
                                                    $icon = get_sub_field('icon', 'option');
                                                    $link = get_sub_field('link', 'option');
                                                    ?>
                                <div class="newsletter__icons-item with-img__right-icons-item ">
                                    <a class="" href="<?php echo $link['url']; ?>"
                                        aria-label=" <?php echo $link['title']; ?>">
                                        <div class="contacts-block-small__icon">
                                            <img src="<?php echo $icon['url']; ?>"
                                                alt="<?php echo $link['title']; ?>" />
                                        </div>
                                    </a>
                                </div>
                                <?php endwhile; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>