       <div class="container-fluid">
           <div class="row">
               <div class="col">
                   <div class="newsletter-icons__container">
                       <?php if (have_rows('usps', 'option')): ?>
                       <div class="newsletter-icons">
                           <?php while (have_rows('usps', 'option')): the_row();
                                        $icon = get_sub_field('usp_icon', 'option');
                                        $title = get_sub_field('usp_title', 'option');
                                        ?>
                           <div class="newsletter-icons__item">
                               <div class="newsletter-icons__icon">
                                   <img src="<?php echo $icon['url']; ?>" alt="<?php echo $title; ?>" />
                               </div>
                               <div class="newsletter-icons__title">
                                   <?php echo $title; ?>
                               </div>
                           </div>
                           <?php endwhile; ?>
                       </div>
                       <?php endif; ?>


                       <?php if (have_rows('brands', 'option')): ?>
                       <div class="newsletter-icons">
                           <?php while (have_rows('brands', 'option')): the_row();
                                        $icon = get_sub_field('brand_icon', 'option');
                                        $link = get_sub_field('brand_link', 'option');
                                        ?>
                           <div class="newsletter-icons__brand-icon">
                               <a href="<?php echo $link['url']; ?>"
                                   aria-label=" <?php echo $link['title']; ?>">
                                   <div>
                                       <img src="<?php echo $icon['url']; ?>" alt="<?php echo $link['title']; ?>" />
                                   </div>
                               </a>
                           </div>
                           <?php endwhile; ?>
                       </div>
                       <?php endif; ?>
                   </div>
               </div>
           </div>
       </div>